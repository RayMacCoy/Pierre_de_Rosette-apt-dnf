# apt <-> dnf <-> pacman

# Pierre de Rosette DNF <-> APT <-> pacman

## Objectifs de la documentation

Faire en sorte qu'une personne maîtrisant correctement le gestionnaire de paquets d'un écosystème Debian, Red Hat ou Archlinux puisse agir sur celui qu'il maîtrise un peu moins.

## Correspondance actions Debian <-> actions Red Hat
| Objectif | Debian | Red Hat | Archlinux |
|----------|--------|---------|-----------|
|Chercher un paquet | Pour chercher *tree*[^1]<br /> `$ apt-cache search tree`[^2] <br />ou<br /> `$ apt search tree`[^3] | Pour chercher *zsh*<br />`$ dnf search zsh`|
|Découvrir le paquet| Pour le paquet `tree`.<br />`$ apt show tree` | Pour le paquet `tree`.<br />`$ dnf info tree`|
|Installer un paquet| Pour le paquet `tree`.<br />`# apt-get install tree`|Pour installer le paquet `zsh`<br />`# dnf install zsh`|
|Supprimer un paquet| Pour le paquet `tree`.<br />`# apt remove tree`|Pour le paquet `zsh`<br />`# dnf remove zsh`
|Supprimer un paquet et ses fichiers de configuration| Pour le paquet `tree`.<br />`# apt purge tree`|
|Télécharger un paquet[^4] sans l'installer|Pour le paquet `tree`.<br />`$ apt download tree`[^5]<br />ou<br />`$ apt-get download tree`|Pour le paquet `zsh`.<br />`$ dnf download zsh`|
|Découvrir le paquet depuis un fichier|`$ dpkg --info <nom_du_fichier.deb>`<br />ou<br />`$ dpkg -I <nom_du_fichier.deb>`|`$ rpm -qip <nom_du_fichier.rpm>`|
|Installer un paquet depuis un fichier|`# dpkg --install <nom_du_fichier.deb>`<br />ou<br />`# dpkg -i <nom_du_fichier.deb>`|`# dnf localinstall <nom_du_fichier.rpm>`
|Mettre à jour le catalogue[^6]|`# apt update`| |
|Mettre à jour complète un système|`# apt update && apt upgrade`|`# dnf update`|
|Lister les paquets installés| `$ dpkg -l` | `dnf list installed`|
|Vérifier l'installation d'un paquet | Pour le paquet `tzdata`<br />`dpkg -l tzdata` | Pour le paquet `tzdata`<br />`dnf list installed tzdata`|
|Lister les paquets à mettre à jour|`$ apt list --upgradable` | `$ dnf list --upgrades`|
|Supprimer les paquets qui ne sont plus indispensables| `# apt autoremove` | `# dnf autoremove` | |

[^1]: `apt-cache search`, au moins pour la recherche du paquet *tree* retourne plus de résultats que `apt search`.

[^2]: La première commande est à préférer pour `greper` la sortie.

[^3]: La seconde commande affiche le résultat sur 2 lignes. La première mentionne l'origine du paquet, sa version et son état installé ou pas. La seconde ligne est une description du paquet.

[^4]: Les dépendances nécessaires au fonctionnement du paquet ne seront pas téléchargées, il vous faudra les récupérer de la même manière si elles venaient à être manquantes.

[^5]: Étonnamment l'option `download` n'est pas mentionnée dans l'aide de la commande `apt` ni même dans sa page de manuel.

[^6]: Les catalogues (`/var/cache/apt/pkgcache.bin` pour les paquets binaires, `/var/cache/apt/srcpkgcache.bin` pour les paquets sources) contiennent toutes les informations des paquets disponibles dans les dépôts configurés.

## Fichiers de configuration

| Écosystème | Fichier | Description |
|------------|---------|-------------|
| Debian | `/etc/apt/sources.list` | Contient les URL et les informations relatives aux dépôts à utiliser.<br />Il est préférable de ne pas y ajouter de nouvelles références mais seulement d'y changer la catégorie des paquets qui peuvent être installés (_main_, _contrib_ ou encore _non-free_) (https://wiki.debian.org/fr/SourcesList ou https://wiki.debian.org/SourcesList). 
| Debian | `/etc/apt/sources.list.d/` fichiers `nom_du_depot.list` | Est au même format que `/etc/apt/sources.list`, contient les dépôts à utiliser.
| Debian | `/etc/apt/sources.list.d/` fichiers `extrepo_<nom_d_un_depôt>.sources` | Contient le dépôt géré par `extrepo`.
| Red Hat| `/etc/yum.repos.d` fichiers `*.repo`| Contient le nom du miroir `name`<br />, l'URL `mirrorlist`<br />, l'indicateur d'utilisation `enabled` (0, désactive le dépôt et 1 l'active)<br />, celui de la vérification de la clé GPG `gpgcheck` et<br /> la clé GPG `gpgkey`
| Red Hat| `/etc/dnf/dnf.conf`

### Utilisation du binaire `extrepo`du paquet du même nom.

`extrepo` ajoute les dépôts selon un critère de recherche.

Exemple :

    # extrepo search teams ## pour chercher Teams

il retourne :

    Found msteams:
    ---
    description: Microsoft Teams collaboration software
    gpg-key-checksum:
      sha256: 4097a14dd56c7d9a44ac164945396c3a460346fca94fe89deebec8468f18c8df
    gpg-key-file: msteams.asc
    policy: non-free
    source:
      Architectures: amd64
      Components: main
      Suites: stable
      Types: deb
      URIs: https://packages.microsoft.com/repos/ms-teams

Il faut que la valeur de `policy` soit celle mentionnée dans le fichier `/etc/extrepo/config.yaml`.

Pour ajouter le dépôt avec l'information mentionnée à la ligne `Found` :

    # extrepo enable msteams 
    
    
    Configuration enabled.

Le fichier `/etc/apt/sources.list.d/extrepo_msteams.sources` sera créé.
Le dépôt est maintenant configuré, pour découvrir les paquets qui y seront disponibles, il faut mettre à jour les catalogues (`# apt update`)

### Utilisation de dnf pour ajouter un dépôt

    # dnf config-manager --add-repo <url_dépôt>.repo

---

